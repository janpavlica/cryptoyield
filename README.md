# CryptoYield

Coinbase crypto investment return.

[UX/UI design in Figma](https://www.figma.com/file/3QKASxRgxA7j1tx9VSaU05/CryptoYield?node-id=0%3A1)\
[Simple prototype in Figma](https://www.figma.com/proto/3QKASxRgxA7j1tx9VSaU05/CryptoYield?node-id=626%3A174&scaling=scale-down&page-id=0%3A1&starting-point-node-id=626%3A174) 

## Features

- Overview of return on investment.
- Overview of purchased assets.
- Overview of transactions on individual assets.
- Overview of payment methods.
- Overview of movements on individual methods.

## Requirements

- iOS 15.0+
- Xcode 11.0

## Author

**Jan Pavlica**\
palivko@gmail.com / [LinkedIn](https://www.linkedin.com/in/jan-pavlica/)

Distributed under the MIT license. See ``LICENSE`` for more information.