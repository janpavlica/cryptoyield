//
// Created by Jan Pavlica on 23.06.2022.
//

import Foundation

enum LoadingState: Int {

    case loading
    case empty
    case success
    case canceled
    case error
}
