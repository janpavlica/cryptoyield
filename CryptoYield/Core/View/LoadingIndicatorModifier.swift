//
// Created by Jan Pavlica on 07.06.2022.
//

import SwiftUI

struct LoadingIndicatorModifier: ViewModifier {

    var isLoading: Bool
    @State private var isAnimation = false

    func body(content: Content) -> some View {
        ZStack {
            content
                    .disabled(isLoading)
                    .blur(radius: isLoading ? 10 : 0)
            if isLoading {
                Circle()
                        .trim(from: 0, to: 0.7)
                        .stroke(Color(.systemGray3), style: StrokeStyle(lineWidth: 8, lineCap: .round,
                                lineJoin: .round))
                        .frame(width: 50, height: 50, alignment: .center)
                        .rotationEffect(Angle(degrees: isLoading && isAnimation ? 360: 0))
                        .onAppear {
                            let animation = Animation.linear.repeatForever(autoreverses: false)
                            withAnimation(animation) {
                                if isLoading {
                                    self.isAnimation.toggle()
                                }
                            }
                        }
            }
        }
    }
}

extension View {
    func loadingIndicatorWrapper(isLoading: Bool) -> some View {
        modifier(LoadingIndicatorModifier(isLoading: isLoading))
    }
}
