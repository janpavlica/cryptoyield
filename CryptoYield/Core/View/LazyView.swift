//
// Created by Jan Pavlica on 23.06.2022.
//

import Foundation
import SwiftUI

struct LazyView<Content: View>: View {

    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    var body: Content {
        build()
    }
}
