//
// Created by Jan Pavlica on 12.07.2022.
//

import Foundation
import SwAuth

struct OAuthEndpoint: Requestable {

    let path: String
    let method: RequestMethod
    let headerParameters: [String: String]
    let queryParameters: [String: Any]
    let authorization: Swauthable

    init(path: String, method: RequestMethod, headerParameters: [String: String] = [:],
         queryParameters: [String: Any] = [:], authorization: Swauthable) {
        self.path = path
        self.method = method
        self.headerParameters = headerParameters
        self.queryParameters = queryParameters
        self.authorization = authorization
    }

    func getData(request: URLRequest) async throws -> Data {
        do {
            let urlRequest = HTTPRequest(from: request)
            let response = try await authorization.authenticatedRequest(for: urlRequest, numberOfRetries: 2)
            return response.data
        } catch URLError.Code.notConnectedToInternet {
            throw NetworkError.offline
        }
    }
}

extension Swauthable {

    func logout() {
        keychain[data: "\(clientID):tokens"] = nil
    }
}

extension HTTPRequest {

    init(from: URLRequest) {

        self.init(endpoint: from.url!)

        additionalHTTPHeaders = from.allHTTPHeaderFields
        httpMethod = getHttpMethod(from: from.httpMethod ?? "GET")
    }

    func getHttpMethod(from: String) -> HTTPMethod {

        switch from.lowercased() {
        case "head":
            return .HEAD
        case "post":
            return .POST
        case "put":
            return .PUT
        case "patch":
            return .PATCH
        case "delete":
            return .DELETE
        default:
            return .GET
        }
    }
}
