//
// Created by Jan Pavlica on 12.07.2022.
//

import Foundation

struct BaseEndpoint: Requestable {

    let path: String
    let method: RequestMethod
    let headerParameters: [String: String]
    let queryParameters: [String: Any]

    init(path: String, method: RequestMethod, headerParameters: [String: String] = [:], queryParameters: [String: Any] = [:]) {
        self.path = path
        self.method = method
        self.headerParameters = headerParameters
        self.queryParameters = queryParameters
    }

    func getData(request: URLRequest) async throws -> Data {
        do {
            let (data, response) = try await URLSession.shared.data(for: request, delegate: nil)
            guard let response = response as? HTTPURLResponse else {
                throw NetworkError.noResponse
            }
            switch response.statusCode {
            case 200...299:
                return data
            case 401:
                throw NetworkError.unauthorized
            default:
                throw NetworkError.unknown
            }
        } catch URLError.Code.notConnectedToInternet {
            throw NetworkError.offline
        }
    }
}
