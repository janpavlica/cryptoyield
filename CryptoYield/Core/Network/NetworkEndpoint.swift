//
// Created by Jan Pavlica on 15.06.2022.
//

import Foundation

protocol Requestable {

    var path: String { get }
    var method: RequestMethod { get }
    var headerParameters: [String: String] { get }
    var queryParameters: [String: Any] { get }

    func urlRequest(config: NetworkConfigurable) throws -> URLRequest
    func getData(request: URLRequest) async throws -> Data
}

extension Requestable {

    func urlRequest(config: NetworkConfigurable) throws -> URLRequest {

        let url = try url(config: config)
        var urlRequest = URLRequest(url: url)
        var allHeaders: [String: String] = config.headers
        headerParameters.forEach { allHeaders.updateValue($1, forKey: $0) }

        urlRequest.allHTTPHeaderFields = allHeaders
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }

    func url(config: NetworkConfigurable) throws -> URL {

        let baseURL = config.url.last != "/" ? config.url + "/" : config.url
        let endpoint = baseURL.appending(path)

        guard var urlComponents = URLComponents(string: endpoint) else { throw RequestError.components }

        var urlQueryItems = [URLQueryItem]()
        queryParameters.forEach {
            urlQueryItems.append(URLQueryItem(name: $0.key, value: "\($0.value)"))
        }
        urlComponents.queryItems = !urlQueryItems.isEmpty ? urlQueryItems : nil

        guard let url = urlComponents.url else { throw RequestError.components }
        return url
    }
}

public enum RequestMethod: String {
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

enum RequestError: Error {
    case components
}
