//
// Created by Jan Pavlica on 23.06.2022.
//

import Foundation

final class MockUtils {

    static func getAsset() -> Asset {

        Asset(id: UUID(), name: "Bitcoin", primary: false, type: .wallet, currency: "BTC", balance: Amount(amount: 0.012345678,
                currency: "BTC"), exchangeBalance: Amount(amount: 8123.456, currency: "CZK"))
    }

    static func getPaymentMethod() -> PaymentMethod {

        PaymentMethod(id: UUID(), type: .creditCard, name: "Master Card", currency: "EUR")
    }

    static func getTransaction() -> Transaction {

        Transaction(id: UUID(), type: .buy, status: .completed, amount: Amount(amount: 0.002345678, currency: "BTC"),
                nativeAmount: Amount(amount: 20, currency: "EUR"), transactionDate: Date())
    }

    static func getTransfer() -> Transfer {

        Transfer(id: UUID(), status: .completed, type: .deposit, paymentMethod: UUID(), transaction: UUID(),
                amount: Amount(amount: 20, currency: "EUR"), subtotal: Amount(amount: 20, currency: "EUR"),
                fee: Amount(amount: 0, currency: "EUR"), exchangeAmount: Amount(amount: 546, currency: "CZK"),
                transferDate: Date(), committed: true)
    }
}
