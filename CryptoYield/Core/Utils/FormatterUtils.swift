//
// Created by Jan Pavlica on 18.03.2022.
//

import Foundation

class FormatterUtils {

    static func getPercentFormatter(minimumFractionDigits: Int) -> NumberFormatter {

        let percentFormatter = NumberFormatter()
        percentFormatter.numberStyle = .percent
        percentFormatter.positivePrefix = "+"
        percentFormatter.minimumFractionDigits = minimumFractionDigits
        return percentFormatter
    }

    static func getCurrencyFormatter(currencyCode: String, minimumFractionDigits: Int) -> NumberFormatter {

        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currencyISOCode
        currencyFormatter.currencyCode = currencyCode
        currencyFormatter.minimumFractionDigits = minimumFractionDigits
        return currencyFormatter
    }

    static func getDateFormatter() -> DateFormatter {

        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        formatter.timeZone = TimeZone.current
        return formatter
    }
}
