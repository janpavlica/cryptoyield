//
// Created by Jan Pavlica on 21.06.2022.
//

import Foundation
import Swinject
import SwAuth

final class DataSourceAssembly: Assembly {

    func assemble(container: Container) {

        container.register(PaymentMethodStorage.self) { _ in
            DefaultPaymentMethodStorage(userDefaults: UserDefaults.standard)
        }

        container.register(CoinbaseApi.self) { _ in

            let keychain = Keychain(service: "com.janpavlica.CryptoYieldApp").label("CryptoYield")

            let authorization: AuthorizationCodeFlow = try! AuthorizationCodeFlow(clientID: Configuration.value(for: "CoinbaseClientId"),
                    clientSecret: Configuration.value(for: "CoinbaseClientSecret"),
                    authorizationEndpoint: URL(string: "https://www.coinbase.com/oauth/authorize")!,
                    tokenEndpoint: URL(string: "https://www.coinbase.com/oauth/token")!,
                    redirectURI: "cryptoyield://oauth2-callback",
                    keychain: keychain,
                    scopes: "wallet:user:read,wallet:user:email,wallet:accounts:read,wallet:payment-methods:read," +
                            "wallet:transactions:read,wallet:buys:read,wallet:sells:read,wallet:deposits:read,wallet:withdrawals:read")
            authorization.additionalAuthorizationParams = [ "account": "all" ]

            return DefaultCoinbaseApi(url: "https://api.coinbase.com", version: "2022-06-12", authorization: authorization)
        }
    }
}
