//
// Created by Jan Pavlica on 21.06.2022.
//

import Swinject

final class UseCaseAssembly: Assembly {

    func assemble(container: Container) {

        container.register(GetAssetsUseCase.self) { resolver in

            guard let assetRepository = resolver.resolve(AssetRepository.self) else {
                fatalError("AssetRepository dependency could not be resolved")
            }
            return GetAssetsUseCase(repository: assetRepository)
        }

        container.register(GetInputUseCase.self) { resolver in

            guard let transferRepository = resolver.resolve(TransferRepository.self) else {
                fatalError("TransferRepository dependency could not be resolved")
            }
            return GetInputUseCase(repository: transferRepository)
        }

        container.register(GetPaymentMethodsUseCase.self) { resolver in

            guard let paymentMethodRepository = resolver.resolve(PaymentMethodRepository.self) else {
                fatalError("PaymentMethodRepository dependency could not be resolved")
            }
            return GetPaymentMethodsUseCase(repository: paymentMethodRepository)
        }

        container.register(GetTransfersUseCase.self) { resolver in

            guard let transferRepository = resolver.resolve(TransferRepository.self) else {
                fatalError("TransferRepository dependency could not be resolved")
            }
            return GetTransfersUseCase(repository: transferRepository)
        }

        container.register(GetTransactionsUseCase.self) { resolver in

            guard let transactionRepository = resolver.resolve(TransactionRepository.self) else {
                fatalError("TransactionRepository dependency could not be resolved")
            }
            return GetTransactionsUseCase(repository: transactionRepository)
        }

        container.register(LoginUseCase.self) { resolver in

            guard let accountRepository = resolver.resolve(AccountRepository.self) else {
                fatalError("AccountRepository dependency could not be resolved")
            }
            return LoginUseCase(repository: accountRepository)
        }

        container.register(GetAccountUseCase.self) { resolver in

            guard let accountRepository = resolver.resolve(AccountRepository.self) else {
                fatalError("AccountRepository dependency could not be resolved")
            }
            return GetAccountUseCase(repository: accountRepository)
        }

        container.register(LogoutUseCase.self) { resolver in

            guard let accountRepository = resolver.resolve(AccountRepository.self) else {
                fatalError("AccountRepository dependency could not be resolved")
            }
            return LogoutUseCase(repository: accountRepository)
        }
    }
}
