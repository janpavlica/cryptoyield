//
// Created by Jan Pavlica on 21.06.2022.
//

import Swinject

final class RepositoryAssembly: Assembly {

    func assemble(container: Container) {

        container.register(AssetRepository.self) { resolver in

            guard let coinbaseApi = resolver.resolve(CoinbaseApi.self) else {
                fatalError("CoinbaseApi dependency could not be resolved")
            }
            return DefaultAssetRepository(coinbase: coinbaseApi)
        }

        container.register(PaymentMethodRepository.self) { resolver in

            guard let coinbaseApi = resolver.resolve(CoinbaseApi.self) else {
                fatalError("CoinbaseApi dependency could not be resolved")
            }
            guard let paymentMethodStorage = resolver.resolve(PaymentMethodStorage.self) else {
                fatalError("PaymentMethodStorage dependency could not be resolved")
            }
            return DefaultPaymentMethodRepository(coinbase: coinbaseApi, storage: paymentMethodStorage)
        }

        container.register(TransferRepository.self) { resolver in

            guard let coinbaseApi = resolver.resolve(CoinbaseApi.self) else {
                fatalError("CoinbaseApi dependency could not be resolved")
            }
            guard let paymentMethodStorage = resolver.resolve(PaymentMethodStorage.self) else {
                fatalError("PaymentMethodStorage dependency could not be resolved")
            }
            return DefaultTransferRepository(coinbase: coinbaseApi, storage: paymentMethodStorage)
        }

        container.register(TransactionRepository.self) { resolver in

            guard let coinbaseApi = resolver.resolve(CoinbaseApi.self) else {
                fatalError("CoinbaseApi dependency could not be resolved")
            }
            return DefaultTransactionRepository(coinbase: coinbaseApi)
        }

        container.register(AccountRepository.self) { resolver in

            guard let coinbaseApi = resolver.resolve(CoinbaseApi.self) else {
                fatalError("CoinbaseApi dependency could not be resolved")
            }
            return DefaultAccountRepository(coinbase: coinbaseApi)
        }
    }
}
