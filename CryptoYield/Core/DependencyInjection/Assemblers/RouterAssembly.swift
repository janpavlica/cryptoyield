//
// Created by Jan Pavlica on 21.06.2022.
//

import Swinject

final class RouterAssembly: Assembly {

    func assemble(container: Container) {

        container.register(MainRouter.self) { _ in

            Router()
        }

        container.register(TransfersRouter.self) { _ in

            Router()
        }

        container.register(TransactionsRouter.self) { _ in

            Router()
        }
    }
}
