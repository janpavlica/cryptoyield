//
// Created by Jan Pavlica on 21.06.2022.
//

import Foundation

@propertyWrapper
struct Inject<T> {

    let wrappedValue: T

    init() {
        wrappedValue = DependencyContainer.shared.resolve()
    }
}
