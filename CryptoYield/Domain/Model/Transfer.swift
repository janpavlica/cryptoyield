//
// Created by Jan Pavlica on 07.04.2022.
//

import Foundation

struct Transfer: Equatable, Identifiable {

    let id: UUID
    let status: TransferStatus
    let type: TransferType
    let paymentMethod: UUID
    let transaction: UUID
    let amount: Amount
    let subtotal: Amount
    let fee: Amount
    var exchangeAmount: Amount?
    let transferDate: Date
    let committed: Bool
}

enum TransferStatus {

    case created
    case completed
    case canceled
}

enum TransferType {

    case deposit
    case withdrawal
    case buy
    case sell
}
