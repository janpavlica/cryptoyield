//
// Created by Jan Pavlica on 16.06.2022.
//

import Foundation

struct ExchangeRates: Equatable {

    let currency: String
    let eur: Amount
    let usd: Amount
    let czk: Amount
}
