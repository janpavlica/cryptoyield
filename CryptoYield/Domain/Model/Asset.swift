//
// Created by Jan Pavlica on 07.04.2022.
//

import Foundation

struct Asset: Equatable, Identifiable {

    let id: UUID
    let name: String
    let primary: Bool
    let type: AssetType
    let currency: String
    let balance: Amount
    var exchangeBalance: Amount?
}

enum AssetType: Int {

    case wallet = 0
    case fiat
    case vault
}

struct Amount: Equatable {

    let amount: Decimal
    let currency: String

    init(amount: Decimal, currency: String) {
        self.amount = amount
        self.currency = currency
    }

    init(amount: String, currency: String) {
        self.amount = Decimal(string: amount) ?? 0
        self.currency = currency
    }
}
