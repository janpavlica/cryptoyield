//
// Created by Jan Pavlica on 14.06.2022.
//

import Foundation

struct Account: Equatable, Identifiable {

    let id: UUID
    let name: String
    let username: String
    let email: String
    let avatarUrl: URL
    let nativeCurrency: String
}
