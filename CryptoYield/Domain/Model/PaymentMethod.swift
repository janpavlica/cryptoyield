//
// Created by Jan Pavlica on 14.06.2022.
//

import Foundation

struct PaymentMethod: Equatable, Identifiable {

    let id: UUID
    let type: PaymentMethodType
    let name: String
    let currency: String
    var assetId: UUID?
}

enum PaymentMethodType {

    case bankAccount
    case fiatAccount
    case creditCard
}
