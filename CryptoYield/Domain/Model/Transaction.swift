//
// Created by Jan Pavlica on 07.04.2022.
//

import Foundation

struct Transaction: Equatable, Identifiable {

    let id: UUID
    let type: TransactionType
    let status: TransactionStatus
    let amount: Amount
    let nativeAmount: Amount
    let transactionDate: Date
}

enum TransactionType {

    case send
    case request
    case transfer
    case trade
    case buy
    case sell
    case fiatDeposit
    case fiatWithdrawal
    case proDeposit
    case proWithdrawal
    case vaultWithdrawal
}

enum TransactionStatus {

    case pending
    case completed
    case failed
    case expired
    case cancelled
    case waitingForSignature
    case waitingForClearing
}
