//
// Created by Jan Pavlica on 16.06.2022.
//

import Foundation

struct GetAssetsUseCase {

    private let repository: AssetRepository

    init(repository: AssetRepository) {

        self.repository = repository
    }

    func execute() async -> Result<[Asset], Error> {
        do {

            let assets = try await repository.getAssets()
            let sortedAssets = assets.sorted { $0.name < $1.name }
            return .success(sortedAssets)

        } catch {

            return .failure(error)
        }
    }
}
