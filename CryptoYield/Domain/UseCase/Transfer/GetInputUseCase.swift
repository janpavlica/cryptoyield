//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

struct GetInputUseCase {

    private let repository: TransferRepository

    init(repository: TransferRepository) {

        self.repository = repository
    }

    func execute() async -> Result<Decimal, Error> {
        do {

            let input = try await repository.getInput()
            return .success(input)

        } catch {

            return .failure(error)
        }
    }
}
