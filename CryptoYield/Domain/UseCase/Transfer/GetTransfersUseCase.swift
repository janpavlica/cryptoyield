//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

struct GetTransfersUseCase {

    private let repository: TransferRepository

    init(repository: TransferRepository) {

        self.repository = repository
    }

    func execute(method: PaymentMethod) async -> Result<[Transfer], Error> {
        do {

            let transfers = try await repository.getTransfers(method: method)
            let sortedTransfers = transfers.sorted { $0.transferDate > $1.transferDate }
            return .success(sortedTransfers)

        } catch {

            return .failure(error)
        }
    }
}
