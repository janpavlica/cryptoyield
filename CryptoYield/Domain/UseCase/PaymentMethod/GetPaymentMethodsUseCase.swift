//
// Created by Jan Pavlica on 28.06.2022.
//

import Foundation

struct GetPaymentMethodsUseCase {

    private let repository: PaymentMethodRepository

    init(repository: PaymentMethodRepository) {

        self.repository = repository
    }

    func execute() async -> Result<[PaymentMethod], Error> {
        do {

            let methods = try await repository.getPaymentMethods()
            let sortedMethods = methods.sorted { $0.name > $1.name }
            return .success(sortedMethods)

        } catch {

            return .failure(error)
        }
    }
}
