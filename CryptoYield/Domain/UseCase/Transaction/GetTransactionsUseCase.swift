//
// Created by Jan Pavlica on 21.06.2022.
//

import Foundation

struct GetTransactionsUseCase {

    private let repository: TransactionRepository

    init(repository: TransactionRepository) {

        self.repository = repository
    }

    func execute(asset: Asset) async -> Result<[Transaction], Error> {
        do {

            let transactions = try await repository.getTransactions(asset: asset)
            let sortedTransactions = transactions.sorted { $0.transactionDate > $1.transactionDate }
            return .success(sortedTransactions)

        } catch {

            return .failure(error)
        }
    }
}
