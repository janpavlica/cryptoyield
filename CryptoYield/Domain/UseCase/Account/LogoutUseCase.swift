//
// Created by Jan Pavlica on 15.07.2022.
//

import Foundation

struct LogoutUseCase {

    private let repository: AccountRepository

    init(repository: AccountRepository) {

        self.repository = repository
    }

    func execute() {

        repository.logout()
    }
}
