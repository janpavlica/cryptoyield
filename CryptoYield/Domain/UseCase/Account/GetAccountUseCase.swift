//
// Created by Jan Pavlica on 12.07.2022.
//

import Foundation

struct GetAccountUseCase {

    private let repository: AccountRepository

    init(repository: AccountRepository) {

        self.repository = repository
    }

    func execute() async -> Result<Account, Error> {
        do {

            let account = try await repository.getAccount()
            return .success(account)

        } catch {

            return .failure(error)
        }
    }
}
