//
// Created by Jan Pavlica on 12.08.2022.
//

import Foundation

struct LoginUseCase {

    private let repository: AccountRepository

    init(repository: AccountRepository) {

        self.repository = repository
    }

    func getAuthorizationUrl() -> URL {

        repository.getAuthorizationUrl()
    }

    func getRedirectUrlScheme() -> String {

        repository.getRedirectUrlScheme()
    }

    func isAuthorized() -> Bool {

        repository.isAuthorized()
    }

    func execute(url: URL) async throws {

        try await repository.authorizationResponseHandler(url: url)
    }
}
