//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

protocol TransferRepository {

    func getInput() async throws -> Decimal
    func getTransfers(method: PaymentMethod) async throws -> [Transfer]
}
