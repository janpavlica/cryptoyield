//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

protocol PaymentMethodRepository {

    func getPaymentMethods() async throws -> [PaymentMethod]
}
