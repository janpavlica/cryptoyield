//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

protocol TransactionRepository {

    func getTransactions(asset: Asset) async throws -> [Transaction]
}
