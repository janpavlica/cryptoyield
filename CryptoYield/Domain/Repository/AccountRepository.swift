//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

protocol AccountRepository {

    func getAuthorizationUrl() -> URL
    func getRedirectUrlScheme() -> String
    func authorizationResponseHandler(url: URL) async throws -> Void
    func getAccount() async throws -> Account
    func isAuthorized() -> Bool
    func logout()
}
