//
// Created by Jan Pavlica on 16.06.2022.
//

import Foundation

protocol AssetRepository {

    func getBalance() async throws -> Decimal
    func getAssets() async throws -> [Asset]
}
