//
// Created by Jan Pavlica on 21.06.2022.
//

import Foundation

struct DefaultTransactionRepository: TransactionRepository {

    private let coinbase: CoinbaseApi

    init(coinbase: CoinbaseApi) {
        self.coinbase = coinbase
    }

    func getTransactions(asset: Asset) async throws -> [Transaction] {

        let transactions = try await coinbase.getTransactions(accountId: asset.id).toDomain()
        return transactions.filter { $0.status == .completed }
    }
}
