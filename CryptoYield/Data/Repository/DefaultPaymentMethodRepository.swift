//
// Created by Jan Pavlica on 28.06.2022.
//

import Foundation

struct DefaultPaymentMethodRepository: PaymentMethodRepository {

    private let coinbase: CoinbaseApi
    private let storage: PaymentMethodStorage

    init(coinbase: CoinbaseApi, storage: PaymentMethodStorage) {
        self.coinbase = coinbase
        self.storage = storage
    }

    func getPaymentMethods() async throws -> [PaymentMethod] {

        let methods = try await coinbase.getPaymentMethods().toDomain()
        let filteredMethods = methods.filter { $0.type == .fiatAccount && $0.assetId != nil }
        storage.savePaymentMethods(methods: filteredMethods.map(PaymentMethodEntity.init))
        return methods
    }
}
