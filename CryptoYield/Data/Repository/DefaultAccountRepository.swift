//
// Created by Jan Pavlica on 12.07.2022.
//

import Foundation

struct DefaultAccountRepository: AccountRepository {

    private let coinbase: CoinbaseApi
    private let authResHand: (URL) async throws -> Void

    init(coinbase: CoinbaseApi) {
        self.coinbase = coinbase
        authResHand = coinbase.getAuthorization().authorizationResponseHandler
    }

    func getAuthorizationUrl() -> URL {
        coinbase.getAuthorization().authorizationURL
    }

    func getRedirectUrlScheme() -> String {
        URL(string: coinbase.getAuthorization().redirectURI)!.scheme!
    }

    func authorizationResponseHandler(url: URL) async throws -> Void {
        try await authResHand(url)
    }

    func getAccount() async throws -> Account {
        let account = try await coinbase.getUser().toDomain()
        return account
    }

    func isAuthorized() -> Bool {
        coinbase.getAuthorization().isAuthorized
    }

    func logout() {
        coinbase.getAuthorization().logout()
    }
}
