//
// Created by Jan Pavlica on 16.06.2022.
//

import Foundation

struct DefaultAssetRepository: AssetRepository {

    private let coinbase: CoinbaseApi

    init(coinbase: CoinbaseApi) {
        self.coinbase = coinbase
    }

    func getBalance() async throws -> Decimal {

        let assets = try await getAssets()
        return assets.reduce(0, { $0 + ($1.exchangeBalance?.amount ?? 0) })
    }

    func getAssets() async throws -> [Asset] {

        let assets = try await coinbase.getAccounts().toDomain()
        let filteredAssets = assets.filter { $0.balance.amount > 0 || $0.type == .fiat }

        return try await withThrowingTaskGroup(of: Asset.self) { group -> [Asset] in

            for asset in filteredAssets {
                group.addTask { [self] in
                    let exchangeRates = try await coinbase.getExchangeRates(currency: asset.currency).toDomain()
                    var newAsset = asset
                    if let rate = exchangeRates?.czk {
                        newAsset.exchangeBalance = Amount(amount: newAsset.balance.amount * rate.amount, currency: rate.currency)
                    }
                    return newAsset
                }
            }

            var exchangeAssets = [Asset]()
            exchangeAssets.reserveCapacity(filteredAssets.count)
            for try await asset in group {
                exchangeAssets.append(asset)
            }
            return exchangeAssets
        }
    }
}
