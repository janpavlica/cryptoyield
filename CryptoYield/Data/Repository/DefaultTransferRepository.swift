//
// Created by Jan Pavlica on 14.06.2022.
//

import Foundation

struct DefaultTransferRepository: TransferRepository {

    private let coinbase: CoinbaseApi
    private let storage: PaymentMethodStorage

    init(coinbase: CoinbaseApi, storage: PaymentMethodStorage) {
        self.coinbase = coinbase
        self.storage = storage
    }

    func getInput() async throws -> Decimal {

        let transfers = try await getAllTransfers()
        return transfers.reduce(0, { $0 + ($1.exchangeAmount?.amount ?? 0) })
    }

    func getTransfers(method: PaymentMethod) async throws -> [Transfer] {

        var transfers: [Transfer] = []

        guard let accountId = method.assetId else {
            return []
        }

        async let deposits = try await coinbase.getDeposits(accountId: accountId).toDomain()
        try await transfers.append(contentsOf: deposits)

        async let withdrawals = try await coinbase.getWithdrawals(accountId: accountId).toDomain()
        try await transfers.append(contentsOf: withdrawals)

        let filteredTransfers = transfers.filter { $0.status == .completed && $0.committed == true }

        return try await withThrowingTaskGroup(of: Transfer.self) { group -> [Transfer] in

            for transfer in filteredTransfers {
                group.addTask { [self] in
                    let exchangeRates = try await coinbase.getExchangeRates(currency: transfer.amount.currency).toDomain()
                    var newTransfer = transfer
                    if let rate = exchangeRates?.czk {
                        newTransfer.exchangeAmount = Amount(amount: newTransfer.amount.amount * rate.amount, currency: rate.currency)
                    }
                    return newTransfer
                }
            }

            var exchangeTransfers = [Transfer]()
            exchangeTransfers.reserveCapacity(filteredTransfers.count)
            for try await transfer in group {
                exchangeTransfers.append(transfer)
            }
            return exchangeTransfers
        }
    }

    private func getAllTransfers() async throws -> [Transfer] {

        var transfers: [Transfer] = []

        let methods = storage.getPaymentMethods()
        for method in methods {

            guard let accountId = method.assetId else {
                return []
            }

            async let deposits = try await coinbase.getDeposits(accountId: accountId).toDomain()
            try await transfers.append(contentsOf: deposits)

            async let withdrawals = try await coinbase.getWithdrawals(accountId: accountId).toDomain()
            try await transfers.append(contentsOf: withdrawals)
        }

        let filteredTransfers = transfers.filter { $0.status == .completed && $0.committed == true }

        return try await withThrowingTaskGroup(of: Transfer.self) { group -> [Transfer] in

            for transfer in filteredTransfers {
                group.addTask { [self] in
                    let exchangeRates = try await coinbase.getExchangeRates(currency: transfer.amount.currency).toDomain()
                    var newTransfer = transfer
                    if let rate = exchangeRates?.czk {
                        newTransfer.exchangeAmount = Amount(amount: newTransfer.amount.amount * rate.amount, currency: rate.currency)
                    }
                    return newTransfer
                }
            }

            var exchangeTransfers = [Transfer]()
            exchangeTransfers.reserveCapacity(filteredTransfers.count)
            for try await transfer in group {
                exchangeTransfers.append(transfer)
            }
            return exchangeTransfers
        }
    }
}
