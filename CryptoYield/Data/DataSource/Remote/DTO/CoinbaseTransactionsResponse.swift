//
// Created by Jan Pavlica on 15.06.2022.
//

import Foundation

struct CoinbaseTransactionsResponse: Decodable {

    let pagination: CoinbasePagination
    let data: [CoinbaseTransaction]?
    let errors: [CoinbaseError]?
}

struct CoinbaseTransaction: Decodable {

    let id: UUID
    let type: CoinbaseTransactionType
    let status: CoinbaseTransactionStatus
    let amount: CoinbaseAmount
    let nativeAmount: CoinbaseAmount
    let createdAt: Date
    let updatedAt: Date

    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case status
        case amount
        case nativeAmount = "native_amount"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

enum CoinbaseTransactionType: String, Decodable {

    case send
    case request
    case transfer
    case trade
    case buy
    case sell
    case fiatDeposit = "fiat_deposit"
    case fiatWithdrawal = "fiat_withdrawal"
    case proDeposit = "pro_deposit"
    case proWithdrawal = "pro_withdrawal"
    case vaultWithdrawal = "vault_withdrawal"
}

enum CoinbaseTransactionStatus: String, Decodable {

    case pending
    case completed
    case failed
    case expired
    case cancelled
    case waitingForSignature = "waiting_for_signature"
    case waitingForClearing = "waiting_for_clearing"
}

extension CoinbaseTransactionsResponse {

    func toDomain() -> [Transaction] {
        data?.map { $0.toDomain() } ?? []
    }
}

extension CoinbaseTransaction {

    func toDomain() -> Transaction {
        .init(id: id, type: type.toDomain(), status: status.toDomain(), amount: amount.toDomain(), nativeAmount: nativeAmount.toDomain(),
                transactionDate: updatedAt)
    }
}

extension CoinbaseTransactionType {

    func toDomain() -> TransactionType {
        switch self {
        case .send: return .send
        case .request: return .request
        case .transfer: return .transfer
        case .trade: return .trade
        case .buy: return .buy
        case .sell: return .sell
        case .fiatDeposit: return .fiatDeposit
        case .fiatWithdrawal: return .fiatWithdrawal
        case .proDeposit: return .proDeposit
        case .proWithdrawal: return .proWithdrawal
        case .vaultWithdrawal: return .vaultWithdrawal
        }
    }
}

extension CoinbaseTransactionStatus {

    func toDomain() -> TransactionStatus {
        switch self {
        case .pending: return .pending
        case .completed: return .completed
        case .failed: return .failed
        case .expired: return .expired
        case .cancelled: return .cancelled
        case .waitingForSignature: return .waitingForSignature
        case .waitingForClearing: return .waitingForClearing
        }
    }
}
