//
// Created by Jan Pavlica on 14.06.2022.
//

import Foundation

struct CoinbasePagination: Decodable {

    let limit: Int?
    let order: String?
    let startingAfter: String?
    let endingBefore: String?

    private enum CodingKeys: String, CodingKey {
        case limit
        case order
        case startingAfter = "starting_after"
        case endingBefore = "ending_before"
    }
}
