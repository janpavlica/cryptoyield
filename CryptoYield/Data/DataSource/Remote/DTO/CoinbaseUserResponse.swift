//
// Created by Jan Pavlica on 12.06.2022.
//

import Foundation

struct CoinbaseUserResponse: Decodable {

    let data: CoinbaseUser?
    let errors: [CoinbaseError]?
}

struct CoinbaseUser: Decodable {

    let id: UUID
    let name: String
    let username: String?
    let email: String
    let avatarUrl: URL
    let nativeCurrency: String

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case email
        case avatarUrl = "avatar_url"
        case nativeCurrency = "native_currency"
    }
}

extension CoinbaseUserResponse {

    func toDomain() -> Account {
        data!.toDomain()
    }
}

extension CoinbaseUser {

    func toDomain() -> Account {
        .init(id: id, name: name, username: username ?? "", email: email, avatarUrl: avatarUrl, nativeCurrency: nativeCurrency)
    }
}
