//
// Created by Jan Pavlica on 10.03.2022.
//

import Foundation

struct CoinbaseError: Decodable {

    let id: String
    let message: String
}
