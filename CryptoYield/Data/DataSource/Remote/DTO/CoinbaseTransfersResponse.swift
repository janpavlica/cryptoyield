//
// Created by Jan Pavlica on 10.06.2022.
//

import Foundation

struct CoinbaseTransfersResponse: Decodable {

    let pagination: CoinbasePagination
    let data: [CoinbaseTransfer]?
    let errors: [CoinbaseError]?
}

struct CoinbaseTransfer: Decodable {

    let id: UUID
    let status: CoinbaseTransferStatus
    let paymentMethod: CoinbaseResourceLink
    let transaction: CoinbaseResourceLink
    let amount: CoinbaseAmount
    let subtotal: CoinbaseAmount
    let fee: CoinbaseAmount
    let createdAt: Date
    let updatedAt: Date
    let resource: CoinbaseTransferType
    let committed: Bool
    let payoutAt: Date

    private enum CodingKeys: String, CodingKey {
        case id
        case status
        case paymentMethod = "payment_method"
        case transaction
        case amount
        case subtotal
        case fee
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case resource
        case committed
        case payoutAt = "payout_at"
    }
}

struct CoinbaseResourceLink: Decodable {

    let id: UUID
    let resource: String
    let resourcePath: String

    private enum CodingKeys: String, CodingKey {
        case id
        case resource
        case resourcePath = "resource_path"
    }
}

enum CoinbaseTransferStatus: String, Decodable {

    case created
    case completed
    case canceled
}

enum CoinbaseTransferType: String, Decodable {

    case deposit
    case withdrawal
    case buy
    case sell
}

extension CoinbaseTransfersResponse {

    func toDomain() -> [Transfer] {
        data?.map { $0.toDomain() } ?? []
    }
}

extension CoinbaseTransfer {

    func toDomain() -> Transfer {
        .init(id: id, status: status.toDomain(), type: resource.toDomain(), paymentMethod: paymentMethod.id, transaction: transaction.id,
                amount: amount.toDomain(), subtotal: subtotal.toDomain(), fee: fee.toDomain(), transferDate: updatedAt, committed: committed)
    }
}

extension CoinbaseTransferStatus {

    func toDomain() -> TransferStatus {
        switch self {
        case .created: return .created
        case .completed: return .completed
        case .canceled: return .canceled
        }
    }
}

extension CoinbaseTransferType {

    func toDomain() -> TransferType {
        switch self {
        case .deposit: return .deposit
        case .withdrawal: return .withdrawal
        case .buy: return .buy
        case .sell: return .sell
        }
    }
}
