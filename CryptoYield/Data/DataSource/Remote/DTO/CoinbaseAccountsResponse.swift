//
// Created by Jan Pavlica on 05.03.2022.
//

import Foundation

struct CoinbaseAccountsResponse: Decodable {

    let pagination: CoinbasePagination
    let data: [CoinbaseAccount]?
    let errors: [CoinbaseError]?
}

struct CoinbaseAccount: Decodable {

    let id: UUID
    let name: String
    let primary: Bool
    let type: CoinbaseAccountType
    let currency: CoinbaseCurrency
    let balance: CoinbaseAmount
    let createdAt: Date
    let updatedAt: Date

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case primary
        case type
        case currency
        case balance
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct CoinbaseCurrency: Decodable {

    let code: String
    let name: String
}

struct CoinbaseAmount: Decodable {

    let amount: String
    let currency: String
}

enum CoinbaseAccountType: String, Decodable {

    case wallet
    case fiat
    case vault
}

extension CoinbaseAccountsResponse {

    func toDomain() -> [Asset] {
        data?.map { $0.toDomain() } ?? []
    }
}

extension CoinbaseAccount {

    func toDomain() -> Asset {
        .init(id: id, name: currency.name, primary: primary, type: type.toDomain(), currency: currency.code, balance: balance.toDomain())
    }
}

extension CoinbaseAccountType {

    func toDomain() -> AssetType {
        switch self {
        case .fiat: return .fiat
        case .wallet: return .wallet
        case .vault: return .vault
        }
    }
}

extension CoinbaseAmount {

    func toDomain() -> Amount {
        .init(amount: amount, currency: currency)
    }
}
