//
// Created by Jan Pavlica on 12.06.2022.
//

import Foundation

struct CoinbasePaymentMethodsResponse: Decodable {

    let data: [CoinbasePaymentMethod]?
    let errors: [CoinbaseError]?
}

struct CoinbasePaymentMethod: Decodable {

    let id: UUID
    let type: CoinbasePaymentMethodType
    let name: String
    let currency: String
    let fiatAccount: CoinbaseFiatAccount?

    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case name
        case currency
        case fiatAccount = "fiat_account"
    }
}

enum CoinbasePaymentMethodType: String, Decodable {

    case achBankAccount = "ach_bank_account"
    case sepaBankAccount = "sepa_bank_account"
    case idealBankAccount = "ideal_bank_account"
    case fiatAccount = "fiat_account"
    case bankWire = "bank_wire"
    case creditCard = "credit_card"
    case secure3dCard = "secure3d_card"
    case worldpayCard = "worldpay_card"
    case eftBankAccount = "eft_bank_account"
    case interac
}

struct CoinbaseFiatAccount: Decodable {

    let id: UUID
}

extension CoinbasePaymentMethodsResponse {

    func toDomain() -> [PaymentMethod] {
        data?.map { $0.toDomain() } ?? []
    }
}

extension CoinbasePaymentMethod {

    func toDomain() -> PaymentMethod {
        guard let assetId = fiatAccount?.id else {
            return .init(id: id, type: type.toDomain(), name: name, currency: currency)
        }
        return .init(id: id, type: type.toDomain(), name: name, currency: currency, assetId: assetId)
    }
}

extension CoinbasePaymentMethodType {

    func toDomain() -> PaymentMethodType {
        switch self {
        case .achBankAccount: return .bankAccount
        case .sepaBankAccount: return .bankAccount
        case .idealBankAccount: return .bankAccount
        case .fiatAccount: return .fiatAccount
        case .bankWire: return .bankAccount
        case .creditCard: return .creditCard
        case .secure3dCard: return .creditCard
        case .worldpayCard: return .creditCard
        case .eftBankAccount: return .bankAccount
        case .interac: return .bankAccount
        }
    }
}
