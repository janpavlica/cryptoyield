//
// Created by Jan Pavlica on 07.03.2022.
//

import Foundation

struct CoinbaseExchangeRatesResponse: Decodable {

    let data: CoinbaseExchangeRate?
    let errors: [CoinbaseError]?
}

struct CoinbaseExchangeRate: Decodable {

    let currency: String
    let rates: CoinbaseRates
}

struct CoinbaseRates: Decodable {

    let EUR: String
    let USD: String
    let CZK: String
}

extension CoinbaseExchangeRatesResponse {

    func toDomain() throws -> ExchangeRates? {
        data?.toDomain()
    }
}

extension CoinbaseExchangeRate {

    func toDomain() -> ExchangeRates {
        .init(currency: currency, eur: Amount(amount: rates.EUR, currency: "EUR"), usd: Amount(amount: rates.USD, currency: "USD"),
                czk: Amount(amount: rates.CZK, currency: "CZK"))
    }
}
