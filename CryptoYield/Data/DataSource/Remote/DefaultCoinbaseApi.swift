//
// Created by Jan Pavlica on 15.06.2022.
//

import Foundation
import SwAuth

protocol CoinbaseApi {

    func getAccounts() async throws -> CoinbaseAccountsResponse
    func getDeposits(accountId: UUID) async throws -> CoinbaseTransfersResponse
    func getWithdrawals(accountId: UUID) async throws -> CoinbaseTransfersResponse
    func getBuys(accountId: UUID) async throws -> CoinbaseTransfersResponse
    func getSells(accountId: UUID) async throws -> CoinbaseTransfersResponse
    func getTransactions(accountId: UUID) async throws -> CoinbaseTransactionsResponse
    func getExchangeRates(currency: String) async throws -> CoinbaseExchangeRatesResponse
    func getPaymentMethods() async throws -> CoinbasePaymentMethodsResponse
    func getUser() async throws -> CoinbaseUserResponse
    func getAuthorization() -> AuthorizationCodeFlow
}

final class DefaultCoinbaseApi: NetworkService, CoinbaseApi {

    private let authorization: AuthorizationCodeFlow

    init(url: String, version: String, authorization: AuthorizationCodeFlow) {

        self.authorization = authorization

        let headers = [ "CB-VERSION": version ]
        let config = NetworkConfiguration(url: url, headers: headers)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        super.init(config: config, decoder: decoder)
    }

    func getAccounts() async throws -> CoinbaseAccountsResponse {

        let queryParameters = ["order": "asc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts", method: .get, queryParameters: queryParameters, authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseAccountsResponse.self)
    }

    func getDeposits(accountId: UUID) async throws -> CoinbaseTransfersResponse {

        let queryParameters = ["order": "desc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts/\(accountId)/deposits", method: .get, queryParameters: queryParameters,
                authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseTransfersResponse.self)
    }

    func getWithdrawals(accountId: UUID) async throws -> CoinbaseTransfersResponse {

        let queryParameters = ["order": "desc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts/\(accountId)/withdrawals", method: .get, queryParameters: queryParameters,
                authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseTransfersResponse.self)
    }

    func getBuys(accountId: UUID) async throws -> CoinbaseTransfersResponse {

        let queryParameters = ["order": "desc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts/\(accountId)/buys", method: .get, queryParameters: queryParameters,
                authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseTransfersResponse.self)
    }

    func getSells(accountId: UUID) async throws -> CoinbaseTransfersResponse {

        let queryParameters = ["order": "desc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts/\(accountId)/sells", method: .get, queryParameters: queryParameters,
                authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseTransfersResponse.self)
    }

    func getTransactions(accountId: UUID) async throws -> CoinbaseTransactionsResponse {

        let queryParameters = ["order": "desc"]
        let endpoint = OAuthEndpoint(path: "v2/accounts/\(accountId)/transactions", method: .get, queryParameters: queryParameters,
                authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseTransactionsResponse.self)
    }

    func getExchangeRates(currency: String) async throws -> CoinbaseExchangeRatesResponse {

        let queryParameters = ["currency": currency]
        let endpoint = BaseEndpoint(path: "v2/exchange-rates", method: .get, queryParameters: queryParameters)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseExchangeRatesResponse.self)
    }

    func getPaymentMethods() async throws -> CoinbasePaymentMethodsResponse {

        let endpoint = OAuthEndpoint(path: "v2/payment-methods", method: .get, authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbasePaymentMethodsResponse.self)
    }

    func getUser() async throws -> CoinbaseUserResponse {

        let endpoint = OAuthEndpoint(path: "v2/user", method: .get, authorization: authorization)
        return try await sendRequest(endpoint: endpoint, responseModel: CoinbaseUserResponse.self)
    }

    func getAuthorization() -> AuthorizationCodeFlow {

        authorization
    }
}
