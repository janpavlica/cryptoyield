//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

struct PaymentMethodEntity: Codable {
    let id: UUID
    var assetId: UUID?
}

extension PaymentMethodEntity {

    init(method: PaymentMethod) {
        id = method.id
        if let assetId = method.assetId {
            self.assetId = assetId
        }
    }
}
