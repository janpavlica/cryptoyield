//
// Created by Jan Pavlica on 17.06.2022.
//

import Foundation

protocol PaymentMethodStorage {

    func getPaymentMethods() -> [PaymentMethodEntity]
    func savePaymentMethods(methods: [PaymentMethodEntity])
}

final class DefaultPaymentMethodStorage: PaymentMethodStorage {

    private let assetsKey = "crypto_yield.payment_methods"
    private var userDefaults: UserDefaults

    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }

    func getPaymentMethods() -> [PaymentMethodEntity] {

        if let assetsData = userDefaults.object(forKey: assetsKey) as? Data {
            if let assets = try? JSONDecoder().decode([PaymentMethodEntity].self, from: assetsData) {
                return assets
            }
        }
        return []
    }

    func savePaymentMethods(methods: [PaymentMethodEntity]) {

        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(methods) {
            userDefaults.set(encoded, forKey: assetsKey)
        }
    }
}
