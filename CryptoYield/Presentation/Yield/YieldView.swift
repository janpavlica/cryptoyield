//
//  Created by Jan Pavlica on 03.03.2022.
//

import SwiftUI

struct YieldView: View {

    @ObservedObject private var viewModel: YieldViewModel

    init() {
        viewModel = YieldViewModel()
    }

    var body: some View {
        NavigationView {
            ZStack {
                Color(.systemGroupedBackground)
                        .ignoresSafeArea()
                if viewModel.assets.isEmpty && viewModel.state != .loading {
                    emptyView()
                } else {
                    listView()
                }
            }
                    .navigationBarTitle(viewModel.title)
        }
                .navigationViewStyle(.stack)
    }

    func listView() -> some View {
        List {
            Section(header: Text("Yield")) {
                yieldView()
            }
            Section(header: Text("Assets")) {
                ForEach(viewModel.assets, id: \.id) { asset in
                    let assetRowViewModel = viewModel.getAssetRowViewModel(asset: asset)
                    if viewModel.showTransactions(asset: asset) {
                        NavigationLink(destination: LazyView(viewModel.router.routeToTransactions(asset: asset))) {
                            AssetRowView(viewModel: assetRowViewModel)
                        }
                    } else {
                        AssetRowView(viewModel: assetRowViewModel)
                    }
                }
                        .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
            }
        }
                .listStyle(.insetGrouped)
                // .loadingIndicatorWrapper(isLoading: viewModel.isLoading)
                .refreshable {
                    await viewModel.getData()
                }
    }

    private func yieldView() -> some View {
        VStack(spacing: 10) {
            Text(viewModel.formattedYield.isEmpty ? "12.34" : viewModel.formattedYield)
                    .font(.title)
                    .foregroundColor(viewModel.yieldColor)
                    .fontWeight(.bold)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
            HStack {
                VStack {
                    Text("Balance")
                            .foregroundColor(Color(.systemGray2))
                            .frame(maxWidth: .infinity, alignment: .leading)
                    Text(viewModel.formattedBalance.isEmpty ? "123456.78" : viewModel.formattedBalance)
                            .font(.title3)
                            .fontWeight(.semibold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                }
                        .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
                VStack {
                    Text("Input")
                            .foregroundColor(Color(.systemGray2))
                            .frame(maxWidth: .infinity, alignment: .leading)
                    Text(viewModel.formattedInput.isEmpty ? "123456.78" : viewModel.formattedInput)
                            .font(.title3)
                            .fontWeight(.semibold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                }
                        .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
            }
        }
                .padding(.vertical, 10)
    }

    @ViewBuilder
    private func emptyView() -> some View {
        Group {
            Text("Your assets are empty.")
        }
    }
}

struct YieldView_Previews: PreviewProvider {
    static var previews: some View {
        YieldView()
    }
}
