//
// Created by Jan Pavlica on 05.03.2022.
//

import Foundation

struct AssetRowViewModel {

    var asset: Asset

    var name: String {
        asset.name
    }

    var iconName: String {
        "\(asset.balance.currency.lowercased()).svg"
    }

    var balance: String {
        let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: asset.balance.currency, minimumFractionDigits: asset.type == .fiat ? 2 : 8)
        guard let formattedAmount = formatter.string(from: asset.balance.amount as NSNumber) else {
            return ""
        }
        return formattedAmount
    }

    var exchangeBalance: String {
        if let exchangeBalance = asset.exchangeBalance {
            if exchangeBalance.currency != asset.balance.currency {
                let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: exchangeBalance.currency, minimumFractionDigits: 2)
                guard let formattedAmount = formatter.string(from: exchangeBalance.amount as NSNumber) else {
                    return ""
                }
                return formattedAmount
            }
        }
        return ""
    }
}
