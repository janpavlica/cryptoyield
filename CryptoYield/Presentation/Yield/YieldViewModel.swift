//
// Created by Jan Pavlica on 03.03.2022.
//

import Foundation
import SwiftUI

@MainActor
final class YieldViewModel: ObservableObject {

    @Published var title = "Coinbase"
    @Published var state: LoadingState = .loading
    @Published var assets: [Asset] = []
    @Published var formattedBalance = ""
    @Published var formattedInput = ""
    @Published var formattedYield = ""
    @Published var yieldColor: Color = Color(UIColor.label)
    @Published var errorMessage = ""

    @EnvironmentObject private var appDelegate: AppDelegate

    @Inject var router: TransactionsRouter

    @Inject private var getInputUseCase: GetInputUseCase
    @Inject private var getAssetsUseCase: GetAssetsUseCase

    private var balance: Decimal = 0
    private var input: Decimal = 0

    private let currency = "CZK"

    init() {

        let mock = MockUtils.getAsset()
        assets.append(mock)

        Task {
            await getData()
        }
    }

    func getAssetRowViewModel(asset: Asset) -> AssetRowViewModel {
        AssetRowViewModel(asset: asset)
    }

    func showTransactions(asset: Asset) -> Bool {
        asset.type != .fiat
    }

    func getData() async {

        let currencyFormatter = FormatterUtils.getCurrencyFormatter(currencyCode: currency, minimumFractionDigits: 2)
        let percentFormatter = FormatterUtils.getPercentFormatter(minimumFractionDigits: 2)

        state = .loading

        let resultAssets = await getAssetsUseCase.execute()
        let resultInput = await getInputUseCase.execute()

        switch resultAssets {
        case .success(let assets):

            self.assets = assets
            balance = assets.reduce(0, { $0 + ($1.exchangeBalance?.amount ?? 0) })
            if let formattedBalance = currencyFormatter.string(from: balance as NSNumber) {
                self.formattedBalance = formattedBalance
            }

        case .failure(let error):

            state = .error
            errorMessage = error.localizedDescription
            print(error)
        }

        switch resultInput {
        case .success(let input):

            self.input = input
            if let formattedInput = currencyFormatter.string(from: input as NSNumber) {
                self.formattedInput = formattedInput
            }

            let yield = input > 0 ? (balance - input) / input : 0
            yieldColor = getYieldColor(yield)
            if let formattedYield = percentFormatter.string(from: yield as NSNumber) {
                self.formattedYield = formattedYield
            }
            state = .success

        case .failure(let error):

            state = .error
            errorMessage = error.localizedDescription
            print(error)
        }
    }

    private func getYieldColor(_ yield: Decimal) -> Color {

        if yield > 0 {
            return .green
        } else if yield < 0 {
            return .red
        } else {
            return Color(UIColor.label)
        }
    }
}
