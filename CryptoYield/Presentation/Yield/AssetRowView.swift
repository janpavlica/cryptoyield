//
// Created by Jan Pavlica on 05.03.2022.
//

import SwiftUI

struct AssetRowView: View {

    private var viewModel: AssetRowViewModel
    private let iconSize = 40.0

    init(viewModel: AssetRowViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        HStack(spacing: 10) {
            icon
            Text(viewModel.name)
                    .font(.headline)
            Spacer()
            balance
        }
        .frame(height: iconSize + 20)
    }

    @ViewBuilder
    private var icon: some View {
        if UIImage(named: viewModel.iconName) != nil {
            Image(uiImage: UIImage(named: viewModel.iconName) ?? UIImage())
                    .resizable()
                    .frame(width: iconSize, height: iconSize)
        } else {
            placeholder
        }
    }

    @ViewBuilder
    private var balance: some View {
        if viewModel.exchangeBalance.isEmpty {
            Text(viewModel.balance)
                    .fontWeight(.semibold)
                    .frame(maxWidth: .infinity, alignment: .trailing)
        } else {
            VStack {
                Text(viewModel.exchangeBalance)
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                Text(viewModel.balance)
                        .foregroundColor(Color(.systemGray2))
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .trailing)
            }
        }
    }

    @ViewBuilder
    private var placeholder: some View {
        Circle().fill(Color(.systemGray5))
                .frame(width: iconSize, height: iconSize)
    }
}

struct AssetRowView_Previews: PreviewProvider {
    static var previews: some View {
        let asset = MockUtils.getAsset()
        let viewModel = AssetRowViewModel(asset: asset)
        AssetRowView(viewModel: viewModel)
    }
}
