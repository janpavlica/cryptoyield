//
// Created by Jan Pavlica on 21.06.2022.
//

import SwiftUI

struct TransactionRowView: View {

    private var viewModel: TransactionRowViewModel
    private let iconSize = 40.0

    init(viewModel: TransactionRowViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        HStack(spacing: 10) {
            icon
            Text(viewModel.date)
                    .font(.headline)
            Spacer()
            amount
        }
                .frame(height: iconSize + 20)
    }

    @ViewBuilder
    private var icon: some View {
        if viewModel.iconName.isEmpty {
            placeholder
        } else {
            Image(systemName: viewModel.iconName)
                    .font(.system(size: iconSize))
                    .foregroundColor(viewModel.iconColor)
                    .frame(width: iconSize, height: iconSize)
        }
    }

    @ViewBuilder
    private var amount: some View {
        if viewModel.nativeAmount.isEmpty {
            Text(viewModel.amount)
                    .fontWeight(.semibold)
                    .frame(maxWidth: .infinity, alignment: .trailing)
        } else {
            VStack {
                Text(viewModel.nativeAmount)
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                Text(viewModel.amount)
                        .foregroundColor(Color(.systemGray2))
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .trailing)
            }
        }
    }

    @ViewBuilder
    private var placeholder: some View {
        Circle().fill(Color(.systemGray5))
                .frame(width: iconSize, height: iconSize)
    }
}

struct TransactionRowView_Previews: PreviewProvider {
    static var previews: some View {
        let transaction = MockUtils.getTransaction()
        let viewModel = TransactionRowViewModel(transaction: transaction)
        TransactionRowView(viewModel: viewModel)
    }
}
