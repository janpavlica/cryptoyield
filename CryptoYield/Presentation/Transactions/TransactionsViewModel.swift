//
// Created by Jan Pavlica on 10.03.2022.
//

import Foundation

@MainActor
final class TransactionsViewModel: ObservableObject {

    @Published var state: LoadingState = .loading
    @Published var transactions: [Transaction] = []
    @Published var errorMessage = ""

    private let asset: Asset
    @Inject private var useCase: GetTransactionsUseCase

    init(asset: Asset) {

        self.asset = asset

        let mock = MockUtils.getTransaction()
        transactions.append(mock)

        Task {
            await getData()
        }
    }

    var title: String {
        asset.name
    }

    func getTransactionRowViewModel(transaction: Transaction) -> TransactionRowViewModel {
        TransactionRowViewModel(transaction: transaction)
    }

    func getData() async {

        state = .loading

        let result = await useCase.execute(asset: asset)
        switch result {
        case .success(let transactions):

            self.transactions = transactions
            state = .success

        case .failure(let error):

            state = .error
            errorMessage = error.localizedDescription
            print(error)
        }
    }
}
