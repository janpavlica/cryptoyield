//
// Created by Jan Pavlica on 10.03.2022.
//

import SwiftUI

struct TransactionsView: View {

    @ObservedObject private var viewModel: TransactionsViewModel

    init(asset: Asset) {

        viewModel = TransactionsViewModel(asset: asset)
    }

    var body: some View {
        ZStack {
            Color(.systemGroupedBackground)
                    .ignoresSafeArea()
            if viewModel.transactions.isEmpty && viewModel.state != .loading {
                emptyView()
            } else {
                listView()
            }
        }
                .navigationBarTitle(viewModel.title)
    }

    func listView() -> some View {
        List(viewModel.transactions, id: \.id) { transaction in
            let transactionRowViewModel = viewModel.getTransactionRowViewModel(transaction: transaction)
            TransactionRowView(viewModel: transactionRowViewModel)
        }
                .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
                .refreshable {
                    await viewModel.getData()
                }
    }

    func emptyView() -> some View {
        Group {
            Text("Your transactions are empty.")
        }
    }
 }

struct TransactionsView_Previews: PreviewProvider {
    static var previews: some View {
        let asset = Asset(id: UUID(), name: "Bitcoin", primary: false, type: .wallet,
                currency: "BTC", balance: Amount(amount: 0.01528685, currency: "BTC"))
        TransactionsView(asset: asset)
    }
}
