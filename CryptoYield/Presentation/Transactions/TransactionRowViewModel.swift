//
// Created by Jan Pavlica on 21.06.2022.
//

import Foundation
import SwiftUI

struct TransactionRowViewModel {

    private let transaction: Transaction

    init(transaction: Transaction) {
        self.transaction = transaction
    }

    var date: String {
        let formatter = FormatterUtils.getDateFormatter()
        let date = formatter.string(from: transaction.transactionDate)
        return date
    }

    var iconName: String {
        switch transaction.type {
        case .proDeposit, .fiatDeposit:
            return "arrow.down.circle.fill"
        case .fiatWithdrawal, .proWithdrawal, .vaultWithdrawal:
            return "arrow.up.circle.fill"
        case .buy:
            return "arrow.down.circle.fill"
        case .sell:
            return "arrow.up.circle.fill"
        case .send:
            return "arrow.up.right.circle.fill"
        case .request:
            return "arrow.down.backward.circle.fill"
        case .transfer, .trade:
            return "arrow.forward.circle.fill"
        }
    }

    var iconColor: Color {
        switch transaction.type {
        case .proDeposit, .fiatDeposit:
            return .blue
        case .fiatWithdrawal, .proWithdrawal, .vaultWithdrawal:
            return .orange
        case .buy:
            return .green
        case .sell:
            return .red
        case .send, .request, .transfer, .trade:
            return .purple
        }
    }

    var amount: String {
        let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: transaction.amount.currency, minimumFractionDigits: 8)
        guard let formattedAmount = formatter.string(from: transaction.amount.amount as NSNumber) else {
            return ""
        }
        return formattedAmount
    }

    var nativeAmount: String {
        let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: transaction.nativeAmount.currency, minimumFractionDigits: 2)
        guard let formattedNativeAmount = formatter.string(from: transaction.nativeAmount.amount as NSNumber) else {
            return ""
        }
        return formattedNativeAmount
    }
}
