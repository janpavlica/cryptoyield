//
// Created by Jan Pavlica on 23.06.2022.
//

import Foundation

typealias MainRouter = YieldRouter & PaymentMethodsRouter & AccountRouter

final class MainViewModel: ObservableObject {

    @Inject var router: MainRouter
}
