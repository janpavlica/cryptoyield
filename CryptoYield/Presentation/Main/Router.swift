//
// Created by Jan Pavlica on 23.06.2022.
//

import SwiftUI

final class Router {

}

protocol YieldRouter {

    func routeToYield() -> YieldView
}

extension Router: YieldRouter {

    func routeToYield() -> YieldView {
        YieldView()
    }
}

protocol PaymentMethodsRouter {

    func routeToPaymentMethods() -> PaymentMethodsView
}

extension Router: PaymentMethodsRouter {

    func routeToPaymentMethods() -> PaymentMethodsView {
        PaymentMethodsView()
    }
}

protocol TransfersRouter {

    func routeToTransfers(method: PaymentMethod) -> TransfersView
}

extension Router: TransfersRouter {

    func routeToTransfers(method: PaymentMethod) -> TransfersView {
        TransfersView(method: method)
    }
}

protocol TransactionsRouter {

    func routeToTransactions(asset: Asset) -> TransactionsView
}

extension Router: TransactionsRouter {

    func routeToTransactions(asset: Asset) -> TransactionsView {
        TransactionsView(asset: asset)
    }
}

protocol AccountRouter {

    func routeToAccount() -> AccountView
}

extension Router: AccountRouter {

    func routeToAccount() -> AccountView {
        AccountView()
    }
}
