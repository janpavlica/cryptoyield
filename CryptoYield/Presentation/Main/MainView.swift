//
// Created by Jan Pavlica on 09.03.2022.
//

import SwiftUI
import Foundation

enum MainTab {
    case yield
    case paymentMethods
    case account
}

struct MainView: View {

    @ObservedObject private var viewModel = MainViewModel()
    @State private var defaultTab = MainTab.yield

    let uiTesting = ProcessInfo.processInfo.arguments.contains("UITesting")

    var body: some View {

        TabView(selection: $defaultTab) {

            viewModel.router.routeToYield()
            .tabItem { Label("Yield", systemImage: "chart.line.uptrend.xyaxis") }
            .tag(MainTab.yield)

            viewModel.router.routeToPaymentMethods()
            .tabItem { Label("Payment methods", systemImage: "creditcard") }
            .tag(MainTab.paymentMethods)

            viewModel.router.routeToAccount()
            .tabItem { Label("Account", systemImage: "person") }
            .tag(MainTab.account)
        }
        // .accentColor(Color.accentColor)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
