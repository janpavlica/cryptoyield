//
// Created by Jan Pavlica on 10.03.2022.
//

import SwiftUI
import Foundation

struct TransfersView: View {

    @ObservedObject private var viewModel: TransfersViewModel

    init(method: PaymentMethod) {

        viewModel = TransfersViewModel(method: method)
    }

    var body: some View {
        ZStack {
            Color(.systemGroupedBackground)
                    .ignoresSafeArea()
            if viewModel.transfers.isEmpty && viewModel.state != .loading {
                emptyView()
            } else {
                listView()
            }
        }
                .navigationBarTitle(viewModel.title)
    }

    func listView() -> some View {
        List(viewModel.transfers, id: \.id) { transfer in
            let transferRowViewModel = viewModel.getTransferRowViewModel(transfer: transfer)
            TransferRowView(viewModel: transferRowViewModel)
        }
                .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
                .refreshable {
                    await viewModel.getData()
                }
    }

    func emptyView() -> some View {
        Group {
            Text("Your transfers are empty.")
        }
    }
}

struct TransfersView_Previews: PreviewProvider {
    static var previews: some View {
        let method = MockUtils.getPaymentMethod()
        TransfersView(method: method)
    }
}
