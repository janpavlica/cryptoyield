//
// Created by Jan Pavlica on 10.03.2022.
//

import Foundation
import Combine

@MainActor
final class TransfersViewModel: ObservableObject {

    @Published var state: LoadingState = .loading
    @Published var transfers: [Transfer] = []
    @Published var errorMessage = ""

    private let method: PaymentMethod
    @Inject private var useCase: GetTransfersUseCase

    init(method: PaymentMethod) {

        self.method = method

        let mock = MockUtils.getTransfer()
        transfers.append(mock)

        Task {
            await getData()
        }
    }

    var title: String {
        method.name
    }

    func getTransferRowViewModel(transfer: Transfer) -> TransferRowViewModel {
        TransferRowViewModel(transfer: transfer)
    }

    func getData() async {

        state = .loading

        let result = await useCase.execute(method: method)
        switch result {
        case .success(let transfers):

            self.transfers = transfers
            state = .success

        case .failure(let error):

            state = .error
            errorMessage = error.localizedDescription
            print(error)
        }
    }
}
