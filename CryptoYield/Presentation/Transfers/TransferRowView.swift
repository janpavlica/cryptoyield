//
// Created by Jan Pavlica on 10.06.2022.
//

import SwiftUI

struct TransferRowView: View {

    private var viewModel: TransferRowViewModel
    private let iconSize = 40.0

    init(viewModel: TransferRowViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        HStack(spacing: 10) {
            icon
            Text(viewModel.paid)
                    .font(.headline)
            Spacer()
            amount
        }
                .frame(height: iconSize + 20)
    }

    @ViewBuilder
    private var icon: some View {
        if viewModel.iconName.isEmpty {
            placeholder
        } else {
            Image(systemName: viewModel.iconName)
                    .font(.system(size: iconSize))
                    .foregroundColor(viewModel.iconColor)
                    .frame(width: iconSize, height: iconSize)
        }
    }

    @ViewBuilder
    private var amount: some View {
        if viewModel.exchangeAmount.isEmpty {
            Text(viewModel.amount)
                    .fontWeight(.semibold)
                    .frame(maxWidth: .infinity, alignment: .trailing)
        } else {
            VStack {
                Text(viewModel.exchangeAmount)
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                Text(viewModel.amount)
                        .foregroundColor(Color(.systemGray2))
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .trailing)
            }
        }
    }

    @ViewBuilder
    private var placeholder: some View {
        Circle().fill(Color(.systemGray5))
                .frame(width: iconSize, height: iconSize)
    }
}

struct TransferRowView_Previews: PreviewProvider {
    static var previews: some View {
        let transfer = MockUtils.getTransfer()
        let viewModel = TransferRowViewModel(transfer: transfer)
        TransferRowView(viewModel: viewModel)
    }
}
