//
// Created by Jan Pavlica on 10.06.2022.
//

import Foundation
import SwiftUI

struct TransferRowViewModel {

    private let transfer: Transfer

    init(transfer: Transfer) {
        self.transfer = transfer
    }

    var paid: String {
        let formatter = FormatterUtils.getDateFormatter()
        let date = formatter.string(from: transfer.transferDate)
        return date
    }

    var iconName: String {
        switch transfer.type {
        case .deposit:
            return "arrow.down.circle.fill"
        case .withdrawal:
            return "arrow.up.circle.fill"
        case .buy:
            return "arrow.down.circle.fill"
        case .sell:
            return "arrow.up.circle.fill"
        }
    }

    var iconColor: Color {
        switch transfer.type {
        case .deposit:
            return .blue
        case .withdrawal:
            return .orange
        case .buy:
            return .blue
        case .sell:
            return .orange
        }
    }

    var amount: String {
        let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: transfer.amount.currency, minimumFractionDigits: 2)
        guard let formattedAmount = formatter.string(from: transfer.amount.amount as NSNumber) else {
            return ""
        }
        return formattedAmount
    }

    var exchangeAmount: String {
        if let exchangeAmount = transfer.exchangeAmount {
            if exchangeAmount.currency != transfer.amount.currency {
                let formatter = FormatterUtils.getCurrencyFormatter(currencyCode: exchangeAmount.currency, minimumFractionDigits: 2)
                guard let formattedAmount = formatter.string(from: exchangeAmount.amount as NSNumber) else {
                    return ""
                }
                return formattedAmount
            }
        }
        return ""
    }
}
