//
// Created by Jan Pavlica on 29.06.2022.
//

import Foundation
import SwiftUI

struct PaymentMethodRowViewModel {

    private let method: PaymentMethod

    init(method: PaymentMethod) {
        self.method = method
    }

    var name: String {
        method.name
    }

    var iconName: String {
        switch method.type {
        case .bankAccount:
            return "building.columns.circle.fill"
        case .fiatAccount:
            return "eurosign.circle.fill"
        case .creditCard:
            return "creditcard.circle.fill"
        }
    }

    var iconColor: Color {
        switch method.type {
        case .bankAccount:
            return .green
        case .fiatAccount:
            return .blue
        case .creditCard:
            return .orange
        }
    }
}
