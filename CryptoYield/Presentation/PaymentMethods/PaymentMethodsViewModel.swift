//
// Created by Jan Pavlica on 28.06.2022.
//

import Foundation

@MainActor
class PaymentMethodsViewModel: ObservableObject {

    @Published var title = "Payment methods"
    @Published var state: LoadingState = .loading
    @Published var methods: [PaymentMethod] = []
    @Published var errorMessage = ""

    @Inject var router: TransfersRouter

    @Inject private var useCase: GetPaymentMethodsUseCase

    init() {

        let mock = MockUtils.getPaymentMethod()
        methods.append(mock)

        Task {
            await getData()
        }
    }

    func getPaymentMethodRowViewModel(method: PaymentMethod) -> PaymentMethodRowViewModel {
        PaymentMethodRowViewModel(method: method)
    }

    func showTransfers(method: PaymentMethod) -> Bool {
        method.type == .fiatAccount && method.assetId != nil
    }

    func getData() async {

        state = .loading

        let result = await useCase.execute()
        switch result {
        case .success(let methods):

            self.methods = methods
            state = .success

        case .failure(let error):

            state = .error
            errorMessage = error.localizedDescription
            print(error)
        }
    }
}
