//
// Created by Jan Pavlica on 29.06.2022.
//

import SwiftUI

struct PaymentMethodRowView: View {

    private var viewModel: PaymentMethodRowViewModel
    private let iconSize = 40.0

    init(viewModel: PaymentMethodRowViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        HStack(spacing: 10) {
            icon
            Text(viewModel.name)
                    .font(.headline)
        }
                .frame(height: iconSize + 20)
    }

    @ViewBuilder
    private var icon: some View {
        if viewModel.iconName.isEmpty {
            placeholder
        } else {
            Image(systemName: viewModel.iconName)
                    .font(.system(size: iconSize))
                    .foregroundColor(viewModel.iconColor)
                    .frame(width: iconSize, height: iconSize)
        }
    }

    @ViewBuilder
    private var placeholder: some View {
        Circle().fill(Color(.systemGray5))
                .frame(width: iconSize, height: iconSize)
    }
}

struct PaymentMethodRowView_Previews: PreviewProvider {
    static var previews: some View {
        let method = MockUtils.getPaymentMethod()
        let viewModel = PaymentMethodRowViewModel(method: method)
        PaymentMethodRowView(viewModel: viewModel)
    }
}
