//
// Created by Jan Pavlica on 28.06.2022.
//

import SwiftUI

struct PaymentMethodsView: View {

    @ObservedObject private var viewModel = PaymentMethodsViewModel()

    var body: some View {
        NavigationView {
            ZStack {
                Color(.systemGroupedBackground)
                        .ignoresSafeArea()
                if viewModel.methods.isEmpty && viewModel.state != .loading {
                    emptyView()
                } else {
                    listView()
                }
            }
                    .navigationBarTitle(viewModel.title)
        }
                .navigationViewStyle(.stack)
    }

    func listView() -> some View {
        List(viewModel.methods, id: \.id) { method in
            let methodRowViewModel = viewModel.getPaymentMethodRowViewModel(method: method)
            if viewModel.showTransfers(method: method) {
                NavigationLink(destination: LazyView(viewModel.router.routeToTransfers(method: method))) {
                    PaymentMethodRowView(viewModel: methodRowViewModel)
                }
            } else {
                PaymentMethodRowView(viewModel: methodRowViewModel)
            }
        }
                .redacted(reason: viewModel.state == .loading ? .placeholder : .init())
                .refreshable {
                    await viewModel.getData()
                }
    }

    func emptyView() -> some View {
        Group {
            Text("Your payment methods are empty.")
        }
    }
}

struct PaymentMethodsView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentMethodsView()
    }
}
