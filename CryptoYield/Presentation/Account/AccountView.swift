//
// Created by Jan Pavlica on 10.03.2022.
//

import Foundation
import SwiftUI
import SwAuth
import BetterSafariView

struct AccountView: View {

    @ObservedObject private var viewModel = AccountViewModel()

    @State private var startingWebAuthenticationSession = false

    private let iconSize = 100.0

    var body: some View {
        NavigationView {
            ZStack {
                Color(.systemGroupedBackground)
                        .ignoresSafeArea()
                Group {
                    VStack(spacing: 20) {
                        avatar
                        if viewModel.isAuthorized {

                            Text(viewModel.name)
                                    .font(.headline)
                            Text(viewModel.email)
                                    .font(.subheadline)
                            Button("Logout") {
                                viewModel.logout()
                            }

                        } else {

                            Button("Login") {
                                self.startingWebAuthenticationSession = true
                            }
                                    .webAuthenticationSession(isPresented: $startingWebAuthenticationSession) {
                                        WebAuthenticationSession(
                                                url: viewModel.authorizationUrl,
                                                callbackURLScheme: viewModel.redirectUrlScheme
                                        ) { callbackUrl, error in
                                            viewModel.login(callbackUrl: callbackUrl, error: error)
                                        }
                                                .prefersEphemeralWebBrowserSession(true)
                                    }
                        }
                    }
                }
                .navigationBarTitle(viewModel.title)
            }
        }
        .navigationViewStyle(.stack)
    }

    @ViewBuilder
    private var avatar: some View {
        if !viewModel.avatarUrl.isEmpty {
            AsyncImage(url: URL(string: viewModel.avatarUrl))
                    .frame(width: iconSize, height: iconSize)
                    .clipShape(Circle())
                    .scaledToFit()
        } else {
            placeholder
        }
    }

    @ViewBuilder
    private var placeholder: some View {
        Circle().fill(Color(.systemGray5))
                .frame(width: iconSize, height: iconSize)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
