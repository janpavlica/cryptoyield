//
// Created by Jan Pavlica on 10.03.2022.
//

import Foundation

@MainActor
final class AccountViewModel: ObservableObject {

    @Published var title = "Account"
    @Published var state: LoadingState = .loading
    @Published var avatarUrl = ""
    @Published var name = ""
    @Published var email = ""
    @Published var errorMessage = ""
    @Published var isAuthorized = false

    @Inject private var loginUseCase: LoginUseCase
    @Inject private var getAccountUseCase: GetAccountUseCase
    @Inject private var logoutUseCase: LogoutUseCase

    var authorizationUrl: URL {
        loginUseCase.getAuthorizationUrl()
    }

    var redirectUrlScheme: String {
        loginUseCase.getRedirectUrlScheme()
    }

    init() {

        isAuthorized = loginUseCase.isAuthorized()

        Task {
            await getData()
        }
    }

    func getData() async {

        if isAuthorized {

            state = .loading

            let result = await getAccountUseCase.execute()
            switch result {
            case .success(let account):

                avatarUrl = account.avatarUrl.absoluteString
                name = account.name
                email = account.email
                state = .success

            case .failure(let error):

                state = .error
                errorMessage = error.localizedDescription
                print(error)
            }
        }
    }

    func login(callbackUrl: URL?, error: Error?) {

        Task.init {
            do {

                guard error == nil else { throw error! }
                try await loginUseCase.execute(url: callbackUrl!)
                isAuthorized = loginUseCase.isAuthorized()
                await getData()

            } catch {

                state = .error
                errorMessage = error.localizedDescription
                print(error)
            }
        }
    }

    func logout() {

        logoutUseCase.execute()
        isAuthorized = false
        avatarUrl = ""
        name = ""
        email = ""
    }
}
