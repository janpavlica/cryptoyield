//
//  CryptoYieldApp.swift
//  CryptoYield
//
//  Created by Jan Pavlica on 03.03.2022.
//

import SwiftUI

@main
struct CryptoYieldApp: App {

    @UIApplicationDelegateAdaptor var appDelegate: AppDelegate

    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
