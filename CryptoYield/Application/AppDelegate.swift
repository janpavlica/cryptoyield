//
// Created by Jan Pavlica on 09.03.2022.
//

import Foundation
import SwiftUI

class AppDelegate: NSObject, UIApplicationDelegate, ObservableObject {

    func application(
            _ application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {

        true
    }
}
